const mongoose = require('mongoose');
require('../models/Models');
const jwt = require('jsonwebtoken');
const secret = 'harrypotter';

const symptom = require('./routeControllers/symptomsController');
const category = require('./routeControllers/categoriesController');
const schedule = require('./routeControllers/scheduleController');
const doctor = require('./routeControllers/doctorsController');
const user = require('./routeControllers/usersController');
const contact = require('./routeControllers/contactController');

//router object, it`s gonna export whatever the route is to that object
//whatever user is trying to access via '/users'
//http://localhost:8080/api/users
module.exports = function (router) {

  //user registration route
  router.post('/users', user.addUser);

  //user login route
  //http://localhost:8080/api/authenticate
  router.post('/authenticate', user.getUser);
  //symptom routes
  router.get('/symptoms', symptom.getAllSymptoms);

  router.post('/symptom', symptom.addSymptom);

  router.get('/symptom/:id', symptom.getSymptomById);

  router.put('/symptom/:id', symptom.editSymptom);

  router.delete('/symptom/:id', symptom.deleteSymptom);

  //category routes
  router.post('/category', category.addCategory);

  router.post('/category/:id/symptoms', category.addSymptomsToCategory);

  router.put('/category/:id', category.editCategory);

  router.get('/categories', category.getAllCategories);

  router.get('/category/:id', category.getCategoryById);

  router.get('/categories/symptoms', category.getAllCategoriesWithSymptoms);

  router.delete('/category/:id/symptoms', category.deleteSymptomFromCategory);

  //schedule routes
  router.get('/doctor/:id/schedule', schedule.getScheduleForDoctor);
  router.post('/schedule', schedule.addScheduleToDoctor);
  router.get('/schedule/:id', schedule.getScheduleById);
  router.put('/schedule/:id', schedule.editSchedule);
  router.delete('/schedule/:id', schedule.deleteSchedule);
  router.post('/schedule/:id', schedule.acceptSchedule);
  router.get('/user/:userEmail/schedule', schedule.getScheduleForUser);

  //doctor routes
  router.post('/doctor/login', doctor.loginDoctor);
  router.get('/doctors', doctor.getAllDoctorsWithPopulatedFields);
  router.get('/doctor/:id', doctor.getDoctorByIdWithPopulatedFields);
  router.post('/doctor', doctor.addDoctor);
  router.post('/doctor/:id/category', doctor.addCategoryToDoctor);
  //contact
  router.post('/contact-us', contact.saveContactMessage);
router.use(function(req, res, next) {

  var token = req.body.token || req.body.query || req.headers['x-access-token'];

  if(token) {
    //verify token
    jwt.verify(token, secret, function(err, decoded) {
      if (err) {
        res.json({ success: false, message: "Token invalid"});
      } else {
        req.decoded = decoded;
        next();
      }
    });
  }else {
    res.json({ success: false, message: "No token provided"});
  }
});

router.post('/me', function(req, res) {
  res.send(req.decoded);
});
  return router;
}