module.exports = (function () {
  const mongoose = require('mongoose');
  const Symptom = mongoose.model('Symptom');
  let publicActions = {};
  
  function addSymptom(req, res) {
    console.log('add new symptom');
    if(req.body.name === undefined ) {
      return res.status(500).json({'message': 'No content sent'});
    }
    let symptom = new Symptom();
    symptom.name = req.body.name;
    symptom.description = req.body.description;
    symptom.save(function (err, newSymptom) {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
      else {
        return res.send(newSymptom);
      }
    })
  }
  
  function getSymptomById(req, res) {
    Symptom.findOne({'_id': req.params.id}, function (err, symptom) {
      if (err) {
        return res.status(500).send(err);
      }
      else {
        return res.status(200).send(symptom);
      }
    })
  }
  
  function getAllSymptoms(req, res) {
    Symptom.find(function (err, symptoms) {
      if (err) {
        return res.status(500).send(err);
      }
      else {
        return res.status(200).send(symptoms);
      }
    })
  }
  
  function editSymptom(req, res) {
    Symptom.findOne({'_id': req.params.id}, function (err, symptom) {
      if (err) {
        return res.status(500).send(err);
      }
      
      symptom.name = req.body.name;
      symptom.description = req.body.description;
      symptom.save(function (err, updatedSymptom) {
        if (err) {
          return res.status(500).send(err);
        }
        return res.status(200).send(updatedSymptom);
      })
    })
  }
  
  function deleteSymptom(req, res) {
    Symptom.remove({'_id': req.params.id}, function(err) {
      if(err) {
        return res.status(500).send(err);
      }
      return res.send({'deleted':'success'});
    })
  }
  
  publicActions = {
    addSymptom: addSymptom,
    getSymptomById: getSymptomById,
    getAllSymptoms: getAllSymptoms,
    editSymptom: editSymptom,
    deleteSymptom: deleteSymptom
  };

  return publicActions;
}());
