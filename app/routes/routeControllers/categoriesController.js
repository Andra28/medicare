module.exports = function () {
    const mongoose = require('mongoose');
    const Category = mongoose.model('Category');
    let publicActions = {};

    function addCategory(req, res) {
        let category = new Category();
        category.name = req.body.name;
        category.description = req.body.description;
        category.save((err, newCategory) => {
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }
            else {
                return res.status(200).send(newCategory);
            }
        })
    }

    function addSymptomsToCategory(req, res) {
        Category.findOne({'_id': req.params.id}, (err, category) => {
            if (err) {
                return res.status(500).send(err);
            }
            if (!category) {
                return res.status(404).send(category);
            }
            else {
                req.body.forEach((symptomId) => {
                    category.symptoms.push(symptomId);
                });
                category.save((err, newCategory) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    else {
                        return res.status(200).send(newCategory);
                    }
                })
            }
        })
    }

    function getCategoryById(req, res) {
        Category.findOne({'_id': req.params.id})
            .populate('symptoms')
            .exec((err, category) => {
                if (err) {
                    return res.status(500).send(err);
                }
                if (!category) {
                    return res.status(404).send(category);
                }
                else {
                    return res.status(200).send(category);
                }
            });
    }

    function editCategory(req, res) {
        Category.findOne({'_id': req.params.id}, (err, category) => {
            if (err) {
                return res.status(500).send(err);
            }
            category.name = req.body.name;
            category.description = req.body.description;
            while (category.symptoms.length > 0) {
                category.symptoms.pop();
            }
            req.body.symptoms.forEach((symptom) => {
                category.symptoms.push(symptom);
            });
            category.save((err, updatedCategory) => {
                if (err) {
                    return res.status(500).send(err);
                }
                else {
                    return res.status(200).send(updatedCategory);
                }
            })
        })
    }

    function getAllCategories(req, res) {
        Category.find((err, categories) => {
            if (err) {
                return res.status(500).send(err);
            }
            else {
                return res.status(200).send(categories);
            }
        })
    }

    function getAllCategoriesWithSymptoms(req, res) {
        Category.find().populate('symptoms')
            .exec((err, categories) => {
                if (err) {
                    return res.status(500).send(err);
                }
                else {
                    return res.status(200).send(categories);
                }
            })

    }

    function deleteCategory(req, res) {
        Category.remove({'_id': req.params.id}, (err) => {
            if (err) {
                return res.status(500).send(err);
            }
            return res.send({'deleted': 'success'});
        })
    }

    function deleteSymptomFromCategory(req, res) {
        Category.findOneAndUpdate({'_id': req.params.id}, {$pullAll: {'symptoms': req.body.deleteSymptoms}},
            (err, category) => {
                if (err) {
                    return res.status(500).send(err);
                }
                else {
                    return res.status(200).send(category);
                }
            })
    }

    function getSymptomByCategories(req, res) {
        //TODO this should be done in front-end
    }

    publicActions = {
        addCategory: addCategory,
        addSymptomsToCategory: addSymptomsToCategory,
        getCategoryById: getCategoryById,
        getAllCategories: getAllCategories,
        editCategory: editCategory,
        deleteCategory: deleteCategory,
        getAllCategoriesWithSymptoms: getAllCategoriesWithSymptoms,
        deleteSymptomFromCategory: deleteSymptomFromCategory
    };

    return publicActions;
}();
