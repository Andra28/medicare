module.exports = (function () {
	const mongoose = require('mongoose');
	const Schedule = mongoose.model('Schedule');
	const Doctor = mongoose.model('Doctor');
	const nodemailer = require('nodemailer');
	const emailTemplate = require('./emailTemplate');

	function addScheduleToDoctor(req, res) {
		if (req.body.title === undefined) {
			return res.status(500).json({'message': 'No content sent'});
		}
		let schedule = new Schedule();
		schedule.medicId = req.body.medicId;
		schedule.title = req.body.title;
		schedule.description = req.body.description;
		schedule.startDate = req.body.startDate;
		schedule.endDate = req.body.endDate;
		schedule.state = "pending";
		schedule.userEmail = req.body.email;

		schedule.save(function (err, newScedule) {
			if (err) {
				console.log(err);
				return res.status(500).send(err);
			}
			else {

				sendMailToDoctor(req.body);
				sendMail(req.body.email);
				return res.send(newScedule);
			}
		})
	}

	function getScheduleForDoctor(req, res) {
		Schedule.find({'medicId': req.params.id}, (err, schedule) => {
			if (err) {
				return res.status(404).send(err);
			}
			else {
				return res.status(200).send(schedule);
			}
		})
	}

	function getScheduleForUser(req, res) {
    Schedule.find({'username': req.params.username}, (err, schedule) => {
      if (err) {
        return res.status(404).send(err);
      }
      else {
        //console(schedule);
        return res.status(200).send(schedule);
      }
    })
  }

	function getScheduleById(req, res) {
		Schedule.findOne({'_id': req.params.id}, (err, schedule) => {
			if (err) {
				return res.status(404).send(err);
			}
			else {
				return res.status(200).send(schedule);
			}

		})
	}


	function editSchedule(req, res) {
		Schedule.findOne({'_id': req.params.id}, (err, schedule) => {
			if (err) {
				res.status(404).send(err);
			}
			else {
				schedule.title = req.body.title;
				schedule.description = req.body.description;
				schedule.startDate = req.body.startDate;
				schedule.endDate = req.body.endDate;
				schedule.save((err, editedSchedule) => {
					if (err) {
						return res.status(500).send(err);
					}
					return res.status(201).send(editedSchedule);
				})
			}
		})
	}


	function acceptSchedule(req, res) {
		Schedule.findOne({'_id': req.params.id}, (err, schedule) => {
			if (err) {
				res.status(404).send(err);
			}
			else {
				schedule.state = "accepted";
				schedule.save((err, editedSchedule) => {
					if (err) {
						return res.status(500).send(err);
					}
					sendMail(schedule.userEmail, emailTemplate());
					return res.status(201).send(editedSchedule);
				})
			}
		})
	}

	function deleteSchedule(req, res) {
		let userEmail = '';
		Schedule.findOne({'_id': req.params.id}, (err, schedule) => {
			userEmail = schedule.userEmail;

			Schedule.findOne({'_id': req.params.id})
				.remove((err, deletedSchedule) => {
					if (err) {
						return res.status(500).send(err);
					}
					else {
						console.log(deletedSchedule);
						sendMail(userEmail, '<h3>Your event was declined!</h3>');
						return res.status(200).send(deletedSchedule);
					}
				})
		});
	}


	function sendMailToDoctor(body) {
		Doctor.findOne({'_id': body.medicId}).exec()
			.then((doctor) => {
				sendMail(doctor.email, '<h3>You have a new schedule!</h3>' )
			})
	}


	function sendMail(userEmail, emailBody) {
		console.log('sending email.............')
		let transporter = nodemailer.createTransport({
			service: 'gmail',
			auth: {
				user: 'ro.medicare@gmail.com',
				pass: 'medicaremedicare'
			}
		});

		let mailOptions = {
			from: '"Medicare" <overnightdev@gmail.com>',
			to: userEmail,
			subject: 'O noua programare a fost creata',
			// text: 'hello',
			html: emailBody
		};

		transporter.sendMail(mailOptions, (error, info) => {
			console.log('executing transport');
			if (error) {
				return console.log(error);
			}
			else {
				console.log('Message %s sent: %s', info.messageId, info.response);
			}
		});
	}


	return {
		addScheduleToDoctor: addScheduleToDoctor,
		getScheduleForDoctor: getScheduleForDoctor,
    getScheduleForUser: getScheduleForUser,
		getScheduleById: getScheduleById,
		editSchedule: editSchedule,
		deleteSchedule: deleteSchedule,
		acceptSchedule: acceptSchedule
	}
})();
