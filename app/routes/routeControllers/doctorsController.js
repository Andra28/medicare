module.exports = (function () {
	const mongoose = require('mongoose');
	const Doctor = mongoose.model('Doctor');


	function loginDoctor(req, res) {
		Doctor.findOne({username: req.body.username}).select('email username password name description phone speciality email').exec(function (err, doctor) {
			if (err) throw err;
			if (!doctor) {
				res.json({success: false, message: 'Could not authenticate doctor'});
			} else if (doctor) {
				if (req.body.password) {
					var providedPassword = req.body.password;
				} else {
					res.json({success: false, message: 'No password provided'});
				}
				if (doctor.comparePassword(providedPassword)) {
					console.log(doctor.comparePassword(providedPassword));
					res.json({success: false, message: 'Could not autenticate with this password!'});
				} else {
					res.json({success: true, message: 'Doctor authenticated', doctor: doctor});
				}
			}
		});
	}

	function addDoctor(req, res) {
		let doctor = new Doctor();
		doctor.username = req.body.username;// take the body of the req and save it on the username variable
		doctor.password = req.body.password;
		doctor.email = req.body.email;
		doctor.name = req.body.name;
		doctor.description = req.body.description;
		doctor.phone = req.body.phone;
		doctor.speciality = req.body.speciality;

		doctor.save((err, newDoctor) => {
			if (err) {
				return res.status(500).send(err);
			}
			else {
				return res.status(200).send(newDoctor);
			}
		})
	}

	function addCategoryToDoctor(req, res) {
		Doctor.findOne({'_id': req.params.id}, (err, doctor) => {
			if (err) {
				return res.status(404).send(err);
			}
			req.body.forEach((categoryId) => {
        doctor.categories.push(categoryId);
			});
			doctor.save((err, newDoctor) => {
				if (err) {
					return res.status(500).send(err);
				}
				else {
					return res.status(200).send(newDoctor);
				}
			})
		})
	}

	function getAllDoctors(req, res) {
		Doctor.find((err, doctors) => {
			if (err) {
				res.status(404).send(err);
			}
			else {
				res.status(200).send(doctors);
			}
		})
	}

	function getAllDoctorsWithPopulatedFields(req, res) {
		Doctor.find()
			.populate('categories')
			.exec((err, doctors) => {
				if (err) {
					return res.status(500).send(err);
				}
				return res.status(200).send(doctors);
			})
	}

	function getDoctorById(req, res) {
		Doctor.findOne({'_id': req.params.id}, (err, doctor) => {
			if (err) {
				res.status(404).send(err);
			}
			else {
				res.status(200).send(doctor);
			}
		})
	}

	function getDoctorByIdWithPopulatedFields(req, res) {
		Doctor.findOne({'_id': req.params.id})
			.populate('categories')
			.exec((err, doctor) => {
				if (err) {
					return res.status(500).send(err);
				}
				if (!doctor) {
					return res.status(404).send(doctor);
				}
				else {
					return res.status(200).send(doctor);
				}
			});
	}

	return {
		loginDoctor: loginDoctor,
		addDoctor: addDoctor,
		getDoctorByIdWithPopulatedFields: getDoctorByIdWithPopulatedFields,
		getAllDoctorsWithPopulatedFields: getAllDoctorsWithPopulatedFields,
		addCategoryToDoctor: addCategoryToDoctor,
		getDoctorById: getDoctorById,
		getAllDoctors: getAllDoctors
	}

})();