module.exports = function () {
  const mongoose = require('mongoose');
  const Contact = mongoose.model('Contact');

  function saveContactMessage(req, res){
    let contact = new Contact();
    contact.subject = req.body.subject;
    contact.name = req.body.name;
    contact.email = req.body.email;
    contact.message = req.body.message;
    contact.save(function(err, newContact) {
      if(err) {
        return res.status(500).send(err);
      }
      res.send(newContact);
    });
  }
  return {
    saveContactMessage: saveContactMessage
  }
}();

