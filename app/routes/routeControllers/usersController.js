module.exports = (function () {
  const jwt = require('jsonwebtoken');
  const mongoose = require('mongoose');
  const User = mongoose.model('User');
  let publicActions = {};
  const secret = 'harrypotter';


  function addUser(req, res) {
    console.log('add new user');
    if (!req.body.username) {
      return res.status(500).json({'message': 'No content sent'});
    }
    let user = new User(); //create a new var
    user.username = req.body.username;// take the body of the req and save it on the username variable
    user.password = req.body.password;
    user.email = req.body.email;
    if (req.body.username == null || req.body.username == '' || req.body.password == null || req.body.password == '' || req.body.email == null || req.body.email == '') {
      res.json({success: false, message: 'Ensure username, email, and password were provided'});//return a json object
    } else {
      user.save(function (err) {
        if (err) {
          res.json({success: false, message: 'Username or Email already exists!'});
        } else {
          res.json({success: true, message: 'user created'});
        }
      });
    }
  }


  function getUser(req, res) {
    User.findOne({username: req.body.username}).select('email username password').exec(function (err, user) {
      if (err) throw err;
      if (!user) {
        res.json({success: false, message: 'Could not authenticate user'});
      } else if (user) {
        if (req.body.password) {
          var validPassword = user.comparePassword(req.body.password);
        } else {
          res.json({success: false, message: 'No password provided'});
        }
        if (!validPassword) {
          res.json({success: false, message: 'Could not autenticate with this password!'});
        } else {
          var token = jwt.sign({username: user.username, email: user.email}, secret, {expiresIn: '24h'});
          res.json({success: true, message: 'User authenticated', token: token});
        }
      }
    });
  }

  publicActions = {
    addUser: addUser,
    getUser: getUser
  };
  return publicActions;
}());