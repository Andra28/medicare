const mongoose = require('mongoose');//invokes mongoose model
const Schema = mongoose.Schema;//mongoose method schema and sets to the schema
const bcrypt = require('bcrypt-nodejs');//pull up our user model

const UserSchema = new Schema({
  username: {type: String, lowercase: true, require: true, unique: true},
  password: {type: String, required: true},
  email: {type: String, require: true, lowercase: true},
  name: {type: String, require: false, lowercase: true}
});

const SymptomSchema = new Schema({
  name: {type: String, lowercase: true, require: true},
  description: {type: String, lowercase: true, require: true}
});

const CategorySchema = new Schema({
  name: {type: String, lowercase: true, require: true},
  description: {type: String, lowercase: true},
  symptoms: [{type: Schema.Types.ObjectId, ref: 'Symptom'}]
});

const ScheduleSchema = new Schema({
  medicId: {type: String, lowercase: true, require: true},
  title: {type: String, lowercase: true},
  description: {type: String, lowercase: true},
  startDate: Schema.Types.Date,
  endDate: Schema.Types.Date,
  state: {type: String, lowercase: true},
  userEmail: {type: String, lowercase: true},
});

const DoctorSchema = new Schema({
  username: {type: String, lowercase: true, require: true, unique: true},
  password: {type: String, required: true},
  email: {type: String, require: true, lowercase: true},
  name: {type: String, lowercase: true, require: true},
  description: {type: String, lowercase: true},
  phone: {type: String, lowercase: true},
  speciality: {type: String, lowercase: true},
  categories: [{type: Schema.Types.ObjectId, ref: 'Category'}],
});
const ContactSchema = new Schema({
  name: {type: String, required: true},
  subject: {type: String, required: true},
  email: {type: String, required: true},
  message: {type: String, required: true},
});

DoctorSchema.pre('save', function (next) {
  let user = this;
  bcrypt.hash(user.password, null, null, function (err, hash) {
    // Store hash in your password DB.
    if (err) return next(err);
    user.password = hash;
    next();
  });
});


DoctorSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};



UserSchema.pre('save', function (next) {
  let user = this;
  bcrypt.hash(user.password, null, null, function (err, hash) {
    // Store hash in your password DB.
    if (err) return next(err);
    user.password = hash;
    next();
  });
});


UserSchema.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

mongoose.model('User', UserSchema); //exports this in the server file
mongoose.model('Symptom', SymptomSchema);
mongoose.model('Category', CategorySchema);
mongoose.model('Schedule', ScheduleSchema);
mongoose.model('Doctor', DoctorSchema);
mongoose.model('Contact', ContactSchema);
