var app = angular.module('userApp')
	.config(function ($routeProvider, $locationProvider) {
		$routeProvider.when('/', {
			templateUrl: 'app/views/home-page/principal.html',
			controller: 'principalCtrl'
		})
			.when('/calendar', {
				templateUrl: 'app/views/schedule/calendarTemplate.html',
				controller: 'calendarController',
        authenticated: true
			})
			.when('/servicii', {
				templateUrl: 'app/views/servicii/servicii.html'
			})
			.when('/contact', {
        templateUrl: 'app/views/contact/contact.html',
				controller:'contactUsController'
      })

			.when('/register', {
				templateUrl: 'app/views/register/register.html',
				controller: 'regCtrl',
				controllerAs: 'register',
				authenticated: false
			})
			.when('/userPage', {
				templateUrl: 'app/views/userPage/userPage.html'
			})
			.when('/login', {
				templateUrl: 'app/views/login/login.html',
				controller: 'loginCtrl',
        authenticated: false
			})
			.when('/doctor-login', {
				templateUrl: 'app/views/doctorLogin/loginDr.html',
				controller: 'loginDrCtrl'
			})

			.when('/doctor-calendar', {
				templateUrl: 'app/views/doctorCalendar/doctorCalendar.html',
				controller: 'doctorCalendarController'
			})
			.when('/logout', {
				templateUrl: 'app/views/logout/logout.html',
				authenticated: true
			})
			.when('/search', {
				templateUrl: 'app/views/search/search.html',
				controller: 'SymptomsSearchCtrl',
        authenticated: true
			})
			.when('/choose-doctor', {
				templateUrl: 'app/views/chooseDoctor/chooseDoctor.html',
				controller: 'chooseDoctorController',
				authenticated: true
			})
			.when('/userProfile', {
				templateUrl: 'app/views/userProfile/userProfile.html',
				controller: 'userProfileCtrl',
				authenticated: true
			});

		// .otherwise('/');
		// {redirectTo: '/'}
		$locationProvider.html5Mode({
			enabled: true,
			requireBase: false
		});
	});

app.run(['$rootScope','authService','$location', function ($rootScope, authService, $location) {

	$rootScope.$on('$routeChangeStart', function (event, next, current) {

		$rootScope.displayLogOutButton = function() {
			return authService.isLoggedIn();
		};

		if(next.$$route.authenticated == true) {
			if(!authService.isLoggedIn()){
				event.preventDefault();
				$location.path('/');
        console.log('needs to be authenticated');
			}

		}else if(next.$$route.authenticated == false){
      if(authService.isLoggedIn()){
        event.preventDefault();
       // $location.path('/');
      console.log('should not be authenticated');
		} else {
      console.log('authentication does not matter');
		}
  };
});

}]);


