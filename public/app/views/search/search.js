angular.module('userApp')
	.controller('SymptomsSearchCtrl', ['$scope', '$http', '$cookies', '$location', 'chooseDoctorService',
		function ($scope, $http, $cookies, $location, chooseDoctorService) {

		$scope.categories = [];
		$scope.doctorWithProvidedCategoryId = [];

		//load events from the server
		$http.get('/api/categories/symptoms').then(function (response) {
			$scope.categories = response.data;
		});

		$scope.findDoctor = function (categoryId) {
			$http.get('/api/doctors').then(function (response) {
				const doctorsArray = response.data;
				$scope.doctorWithProvidedCategoryId = doctorsArray.filter(function (doctor) {
					for (let category of doctor.categories) {
						if (categoryId === category._id) {
							return true;
						}
					}
				})
			})
				.then(function () {
					$scope.foundDoctor = $scope.doctorWithProvidedCategoryId[0];
          chooseDoctorService.set($scope.doctorWithProvidedCategoryId);
					$location.path("/choose-doctor");
				});
		};

		$scope.symptom = function (message) {
			if ($scope.symptoms) {
				return $scope.symptoms.replace(/\s*,\s*/g, ',').split(',').every(function (userInputSearchSymptom) {
					return message.symptoms.some(function (filteredSymptom) {
						return filteredSymptom.name.indexOf(userInputSearchSymptom) !== -1;
					});
				});
			}
			else {
				return true;
			}
		};

		$scope.addSymptomToSearchField = function (symptomName) {
			if (!$scope.symptoms) {
				$scope.symptoms = symptomName;
			}
			else {
				$scope.symptoms = $scope.symptoms + ', ' + symptomName;
			}
		}
	}]);

