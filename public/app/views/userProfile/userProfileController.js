angular.module('userApp')
  .controller('userProfileCtrl', function ($scope, $http, $filter, uiCalendarConfig,authService, $uibModal, $cookies) {
    console.log('am ajuns la userProfile controller');

    $scope.SelectedEvent = null;
    var isFirstTime = true;

    $scope.events = [];
    $scope.eventSources = [$scope.events];
    let modalInstance;


    function loadEvents() {
      authService.getUser().then(function (data) {
        console.log(data.data.username);
        $scope.userEmail = data.data.email;
      })
      $http.get('api/user/' + $scope.userEmail + ' /schedule')
        .then(function (data) {
          console.log(data);
          //clearCalendar();
          $scope.events.slice(0, $scope.events.length);
          angular.forEach(data.data, function (value) {
            $scope.events.push({
              eventId: value._id,
              title: value.title,
              description: value.description,
              start: moment.utc(value.startDate).local(),
              end: moment.utc(value.endDate).local(),
            });
          });
        });
    }

    function clearCalendar() {
      if (uiCalendarConfig.calendars.myCalendar != null) {
        uiCalendarConfig.calendars.myCalendar.fullCalendar('removeEvents');
        uiCalendarConfig.calendars.myCalendar.fullCalendar('unselect');
      }
    }

    loadEvents();

    $scope.dayClick = function (date, jsEvent, view) {
      delete $scope.event;
      let selectedDate = moment(date._d).format('YYYY-MM-DD');
      console.log('formatted date', selectedDate);
      $scope.selectedDate = selectedDate;
      modalInstance = showModal(selectedDate, null);
      modalInstance.result.then((data) => handleOpenModalEvent(data));
    };

    $scope.eventClick = function (event) {
      $scope.event = event;
      console.log('event clicked', event);
      $scope.SelectedEvent = event;
      let fromDate = moment(event.start).format('YYYY/MM/DD LT');
      let endDate = moment(event.end).format('YYYY/MM/DD LT');
      $scope.event = {
        eventId: event.eventId,
        start: fromDate,
        end: endDate,
        IsFullDay: false,
        title: event.title,
        description: event.description
      };
      modalInstance = showModal(fromDate, event);

      modalInstance.result.then((data) => handleOpenModalEvent(data));
    };

    function showModal(selectedDate, event) {
      console.log('opening modal');
      return $uibModal.open($scope.option);
    }

    $scope.uiConfig = {
      calendar: {
        height: 500,
        editable: true,
        displayEventTime: true,
        selectable: true,
        selectHelper: true,
        dayClick: $scope.dayClick,
        header: {
          left: 'month basicWeek basicDay agendaWeek agendaDay',
          center: 'title',
          right: 'today prev,next'
        },
        timeFormat: {
          month: ' ', // for hide on month view
          agenda: 'h:mm t'
        },
        eventClick: $scope.eventClick,
        eventAfterAllRender: function () {
          if ($scope.events.length > 0 && isFirstTime) {
            uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoData', $scope.events[0].start);
            isFirstTime = false;
          }
        }
      }
    };
    $scope.option = {
      animation: true,
      templateUrl: "userProfileModalContent.html",
      controller: 'userProfileModal',
      controllerAs: '$ctrl',
      defaultDate: $scope.selectedDate,
      resolve: {
        event: function () {
          return $scope.event;
        },
        selectedDate: function () {
          return $scope.selectedDate;
        }
      }
    };
  })