angular.module('userApp')
  .controller('chooseDoctorController', ['$scope', '$http', 'chooseDoctorService', '$location', '$cookies', function ($scope, $http, chooseDoctorService, $location, $cookies) {

    $scope.doctorsArray = chooseDoctorService.get();

    $scope.selectDoctor = function (doctor) {
      chooseDoctorService.setSelectedDoctor(doctor);
      $location.path('/calendar');
    }
  }]);
