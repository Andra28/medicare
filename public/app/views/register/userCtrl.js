angular.module('userControllers', ['userServices'])

  .controller('regCtrl', function ($http, $location, $timeout, User) {
    var app = this;

    this.regUser = function(regData) {
      app.loading = true;
      app.errorMsg = false;
      console.log(regData);
      
      User.create(app.regData).then(function (data) {
        if(data.data.success) {
          app.loading = false;
          //create a success message
          app.successMsg = data.data.message + 'Redirecting...';
          //Redirect to the patologies page
          $timeout(function() {
            $location.path('/search');
          }, 1000);
        } else {
          //create an error message
          app.loading = true;
          app.errorMsg = data.data.message;
        }
      });
    }
  });
