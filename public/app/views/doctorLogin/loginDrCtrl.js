angular.module('userApp')
	.controller('loginDrCtrl', function (authService, $timeout, $location, $http, $scope, $cookies, $window) {
		var app = this;
		console.log('am intrat in doctor login controller');
		if (authService.isLoggedIn()) {
			console.log("Success: User is logged in.");
		} else {
			console.log("Failure: User is NOT logged in.");
		}
		$scope.login = {};
		$scope.login.username = '';
		$scope.login.password = '';

		$scope.submit = function () {
			console.log('submit clicked');
			if ($scope.login) {
				return $http.post('/api/doctor/login', $scope.login).then((response) => {
					console.log(response);
					if (response.data.success) {
						$cookies.put('loggedDoctorId', response.data.doctor._id);
						$cookies.put('doctorName', response.data.doctor.name);
						$location.path('/doctor-calendar');
						$scope.login.successMsg = response.data.message;
					}
					else {
						$scope.login.errorMsg = response.data.message;
					}
				})
			}
		};

		//
		// this.loginUser = function (loginData) {
		// 	app.loading = true;
		// 	app.errorMsg = false;
		// 	console.log(loginData);
		// 	authService.js.login(app.loginData).then(function (data) {
		// 		if (data.data.success) {
		// 			app.loading = false;
		// 			//create a success message
		// 			app.successMsg = data.data.message + 'Redirecting...';
		// 			//Redirect to the patologies page
		// 			$timeout(function () {
		// 				$location.path('/drCalendar');
		// 			}, 1000);
		// 		} else {
		// 			//create an error message
		// 			app.loading = true;
		// 			app.errorMsg = data.data.message;
		// 		}
		// 	});
		// };
		// this.logout = function () {
		// 	authService.js.logout();
		// 	$location.path('/logout');
		// 	$timeout(function () {
		// 		$location.path('/');
		// 	}, 1000);
		// };
	});

