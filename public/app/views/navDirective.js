angular.module('userApp', []).directive('isActiveNav', [ '$location', function ($location) {
return {
  restrict: 'A',
  link: function (scope, element) {
    scope.location = $location;
    scope.$watch('location.path()', function (currentPath) {
      let currentPathString =  currentPath;
      let comparePathString = element[0].attributes['href'].textContent;
      if(currentPathString == comparePathString){
        element.parent().addClass('active');
      } else{
        element.parent().removeClass('active');
      }
    });

  }
}
}])