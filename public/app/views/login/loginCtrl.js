angular.module('userApp')

.controller('loginCtrl', function (authService, $timeout, $location, $scope, $rootScope) {
  var app = this;
  $scope.isLoggedIn = function(){

  };
  
  $rootScope.$on('$routeChangeStart', function () {
    if(authService.isLoggedIn()){
      console.log("Success: User is logged in.");
      authService.getUser().then(function (data) {
        console.log(data.data);
        console.log(data.data.username);

        $scope.data = data.data.username;
      })
    }else {
      console.log("Failure: User is NOT logged in.");
      app.username = '';
    }
  });

  this.loginUser = function(loginData) {
    app.loading = true;
    app.errorMsg = false;
    console.log(loginData);

    authService.login(app.loginData).then(function(data) {
      if(data.data.success) {
        app.loading = false;
        //create a success message
        app.successMsg = data.data.message + 'Redirecting...';
        //Redirect to the patologies page
        $timeout(function() {
          $location.path('/search');
          app.loginData = '';
          app.successMsg = false;
        }, 1000);
      } else {
        //create an error message
        app.loading = true;
        app.errorMsg = data.data.message;
      }
    });
  };
$scope.logout = function () {
    authService.logout();
    console.log("Logout");
    $location.path('/');
  };
})
.directive('isActiveNav', [ '$location', function ($location) {
  return {
    restrict: 'A',
    link: function (scope, element) {
      scope.location = $location;
      scope.$watch('location.path()', function (currentPath) {
        let currentPathString =  currentPath;
        let comparePathString = element[0].attributes['href'].textContent;
        if(currentPathString == comparePathString){
          element.parent().addClass('active');
        } else{
          element.parent().removeClass('active');
        }
      });

    }
  }
}]);

