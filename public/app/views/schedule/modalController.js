angular.module('userApp')
	.controller('modalController', ['$scope', '$uibModalInstance', 'event', 'selectedDate', function ($scope, $uibModalInstance, event, selectedDate) {
		console.log('modalController:', event);
		if (!event) {
			$scope.event = {};
			$scope.startDate = new Date(selectedDate).toISOString();
			$scope.endDate = new Date(new Date(selectedDate).getTime() + 30 * 60000).toISOString();
			console.log('start', $scope.event.start, 'end', $scope.event.end);
			$scope.displaySave = true;
			$scope.email = '';
		}
		if (event) {
			$scope.displaySave = false;
		}

		$scope.event = event;
		$scope.hstep = 1;
		$scope.mstep = 30;

		$scope.Message = '';
		$scope.ismeridian = true;

		$scope.toggleMode = function () {
			$scope.ismeridian = !$scope.ismeridian;
		};

		$scope.dateOptions = {
			maxDate: new Date(2020, 5, 22)
		};

		$scope.ok = function () {
			if (!$scope.event.title) {
				$scope.Message = "Este necesar un titlu";
			}
			else if(!$scope.event.description) {
				$scope.Message = "Descrierea este necesara!";
			}
			else if (!$scope.event.email) {
				$scope.Message = "Emailul este necesar!";
			} else if (!$scope.event.startTime && $scope.event.endTime) {
				$scope.Message = "Ora ce incepere si terminare a consultatiei sunt necesare!";
			}
			else {
				$uibModalInstance.close({
					event: $scope.event,
					start: $scope.startDate,
					end: $scope.endDate,
					email: $scope.email,
					operation: 'Save'
				});
			}
		};
		$scope.delete = function () {
			$uibModalInstance.close({event: $scope.event, start: $scope.startDate, end: $scope.endDate, operation: 'Delete'});
		};
		$scope.cancel = function () {

			$uibModalInstance.dismiss('cancel');
		};


		$scope.changed = function (date) {
			// $scope.event.start = new Date(date);
			console.log('date changed', date);
		}
		$scope.updateEndDate = function (date) {
			$scope.endDate = new Date(date.getTime() + 30 * 60000).toISOString();
		}


		$scope.addToCalendar = function () {
			var myCalendar = createCalendar({
				options: {
					class: 'my-class',
					id: 'my-id'
				},
				data: {
					// Event title
					title: `Programare Medicare`,

					// Event start date
					start: $scope.startDate ? new Date($scope.startDate) : new Date($scope.event.start),

					// Event duration (IN MINUTES)
					duration: 30,

					// You can also choose to set an end time
					// If an end time is set, this will take precedence over duration
					end: $scope.endDate ? new Date($scope.endDate) : new Date($scope.event.end),

					// Event Address
					address: 'The internet',

					// Event Description
					description: 'Programare Medicare '
				}
			});
			let element = document.querySelector('#add-to-calendar');
			if (element.childElementCount === 0) {
				element.appendChild(myCalendar);
			}
		};

	}]);