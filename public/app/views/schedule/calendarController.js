angular.module('userApp')
	.controller('calendarController', function ($scope, $http, $filter, uiCalendarConfig, $uibModal, chooseDoctorService) {
		console.log('am ajuns la myng controller');
		$scope.SelectedEvent = null;
		var isFirstTime = true;

		$scope.events = [];
		$scope.eventSources = [$scope.events];
		let modalInstance;

		function loadEvents() {
      const selectedDoctor = chooseDoctorService.getSelectedDoctor();
      console.log(selectedDoctor);
			$http.get('api/doctor/' + selectedDoctor._id + '/schedule')
				.then(function (data) {
					clearCalendar();
					$scope.events.slice(0, $scope.events.length);
					angular.forEach(data.data, function (value) {
						$scope.events.push({
							eventId: value._id,
							title: value.title,
							description: value.description,
							start: moment.utc(value.startDate).local(),
							end: moment.utc(value.endDate).local(),
              state: value.state,
						});
					});
				});
		}

		function clearCalendar() {
			if (uiCalendarConfig.calendars.myCalendar != null) {
				uiCalendarConfig.calendars.myCalendar.fullCalendar('removeEvents');
				uiCalendarConfig.calendars.myCalendar.fullCalendar('unselect');
			}
		}

		loadEvents();

		$scope.dayClick = function (date, jsEvent, view) {
			delete $scope.event;
			let selectedDate = moment(date._d).format('YYYY-MM-DD');
			console.log('formatted date', selectedDate);
			$scope.selectedDate = selectedDate;
			modalInstance = showModal(selectedDate, null);
			modalInstance.result.then((data) => handleOpenModalEvent(data));
		};

		$scope.eventClick = function (event) {
			$scope.event = event;
			console.log('event clicked', event);
			$scope.SelectedEvent = event;
			let fromDate = moment(event.start).format('YYYY/MM/DD LT');
			let endDate = moment(event.end).format('YYYY/MM/DD LT');
			$scope.event = {
				eventId: event.eventId,
				start: fromDate,
				end: endDate,
				IsFullDay: false,
				title: event.title,
				description: event.description,
        state: event.state
			};
			modalInstance = showModal(fromDate, event);

			modalInstance.result.then((data) => handleOpenModalEvent(data));
		};

		function showModal(selectedDate, event) {
			console.log('opening modal');
			return $uibModal.open($scope.option);
		}

		$scope.uiConfig = {
			calendar: {
				height: 500,
				editable: true,
				displayEventTime: true,
				selectable: true,
				selectHelper: true,
				dayClick: $scope.dayClick,
				header: {
					left: 'month basicWeek basicDay agendaWeek agendaDay',
					center: 'title',
					right: 'today prev,next'
				},
				timeFormat: {
					month: ' ', // for hide on month view
					agenda: 'h:mm t'
				},
				eventClick: $scope.eventClick,
				eventAfterAllRender: function () {
					if ($scope.events.length > 0 && isFirstTime) {
						uiCalendarConfig.calendars.myCalendar.fullCalendar('gotoData', $scope.events[0].start);
						isFirstTime = false;
					}
				},
        eventRender: function (event, element) {
				  console.log("Enter eventRender");

          if (event.state == "accepted") {
            if(!element.a) console.log('elementul nu exista: ', element, "eventul este: ", event );
            element.context.className = element.context.className + ' acceptedEvent';
            console.log(event.state);
            console.log(element);
          }
        },
			}
		};
		$scope.option = {
			animation: true,
			templateUrl: "modalContent.html",
			controller: 'modalController',
			controllerAs: '$ctrl',
			defaultDate: $scope.selectedDate,
			resolve: {
				event: function () {
					return $scope.event;
				},
				selectedDate: function () {
					return $scope.selectedDate;
				}
			}
		};

		function handleOpenModalEvent(data) {
			console.log(data);
			$scope.newEvent = data.event;

			switch (data.operation) {
				case 'Save':            //save
					sendEvent(data.event, data.start, data.end);
					break;
				case 'Delete':          //delete
					deleteEvent(data.event);
					break;
				default:
					break;
			}
		}

		function sendEvent(rawEvent, start, end) {
			console.log('sending new event');
			let normalizedEvent = {
				medicId: chooseDoctorService.getSelectedDoctor()._id,
				title: rawEvent.title,
				description: rawEvent.description,
				startDate: start,//moment.utc(rawEvent.start),
				endDate: end, //moment.utc(rawEvent.end),
				email: rawEvent.email
			};
			console.log('startDate', normalizedEvent.startDate, 'end date', normalizedEvent.endDate);

			$http({
				method: 'POST',
				url: '/api/schedule',
				data: normalizedEvent
			}).then(function (response) {
				console.log('response', response);
				loadEvents();
				//TODO refresh calendar?
			})
		}

		function deleteEvent(rawEvent) {
			console.log('deleting event with id: ' + rawEvent.eventId);
			$http({
				method: 'DELETE',
				url: '/api/schedule/' + rawEvent.eventId
			}).then(function (response) {
				console.log(response);
				loadEvents();
			})
		}
	});

