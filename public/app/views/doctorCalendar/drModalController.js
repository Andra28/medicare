angular.module('userApp')
	.controller('doctorModalController', ['$scope', '$uibModalInstance', 'event', 'selectedDate', function ($scope, $uibModalInstance, event, selectedDate) {
		console.log('modalController:', event);
		if(!event) {
			$scope.event = {};
			$scope.startDate = new Date(selectedDate).toISOString();
			$scope.endDate = new Date(selectedDate).toISOString();
			console.log('start', $scope.event.start, 'end', $scope.event.end);
			$scope.displaySave = true;
			$scope.state = "pending";
		}
		if(event) {
			$scope.displaySave = false;
		}


		$scope.event = event;
		$scope.hstep = 1;
		$scope.mstep = 30;

		$scope.Message = '';
		$scope.ismeridian = true;

		$scope.toggleMode = function() {
			$scope.ismeridian = ! $scope.ismeridian;
		};

		$scope.dateOptions = {
			maxDate: new Date(2020, 5, 22)
		};

		$scope.ok = function () {
			if (!$scope.event.title) {
				$scope.Message = "Este necesar un titlu";
			}
			// } else if (!$scope.event.email) {
			// 	$scope.Message = "Emailul este necesar!";
			// } else if (!$scope.event.startTime && $scope.event.endTime) {
			// 	$scope.Message = "Ora ce incepere si terminare a consultatiei sunt necesare!";
			// }
			else {
				$uibModalInstance.close({event: $scope.event, start: $scope.startDate, end: $scope.endDate, operation: 'Save'});
			}
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		$scope.accept = function () {
      $uibModalInstance.close({event: $scope.event, operation: 'Accept'});
    };
    $scope.decline = function () {
      $uibModalInstance.close({event: $scope.event, operation: 'Decline'});
    };


		$scope.changed = function(date) {
			// $scope.event.start = new Date(date);
			console.log('date changed', date);
		}
	}]);