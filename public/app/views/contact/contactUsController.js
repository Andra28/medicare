angular.module('userApp').controller("contactUsController", function ($http, $scope, $window) {

  $scope.contact = {};
  $scope.contact.name = '';
  $scope.contact.subject = '';
  $scope.contact.message = '';
  $scope.operationSuccess = false;
  $scope.operationError = false;
  console.log("Am intrat in contactUSController");
  $scope.submitContact = function () {
    $http.post('api/contact-us', $scope.contact).success(function (response) {
      $window.location.reload();
      $scope.operationSuccess = true;

    }).error(function (response) {
      $scope.operationError = true;
      alert("Message Not Sent!");
    })
  }
})
