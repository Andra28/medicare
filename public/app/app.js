var app = angular.module('userApp', ['ngRoute', 'ui.calendar', 'userControllers', 'userServices'
  , 'authServices', 'ui.bootstrap', 'ngCookies'])

.config(function($httpProvider){
  $httpProvider.interceptors.push('AuthInterceptors');
});