angular.module('authServices', [])

  .factory('authService', function ($http, AuthToken) {
    let authFactory = {};

    ///user.create(regData)
    authFactory.login = function(loginData) {
      return $http.post('api/authenticate', loginData).then(function(data) {
        AuthToken.setToken(data.data.token);
        return data;
      });
    };

    //Auth.isLoggedIn();
    authFactory.isLoggedIn = function () {
      if(AuthToken.getToken()) {
        return true;
      } else {
        return false;
      }
    };

    authFactory.getUser = function () {
      if(AuthToken.getToken()) {
        return $http.post('/api/me');
      } else {
        $q.reject({ message: 'User has no token' });
      }

    };
    //logout!!!
    authFactory.logout = function () {
      AuthToken.setToken();
    };
    return authFactory;
  })

.factory('AuthToken', function ($window) {
  let authTokenFactory = {};

  //AuthToken.setToken(token);
  authTokenFactory.setToken = function (token) {
    if(token){
      $window.localStorage.setItem('token', token);
    }else{
      $window.localStorage.removeItem('token');
    }
  };

  //AuthToken.getToken();
  authTokenFactory.getToken = function () {
    return $window.localStorage.getItem('token');
  };

  return authTokenFactory;

})

.factory('AuthInterceptors', function (AuthToken) {
  let authInterceptorsFactory = {};

  authInterceptorsFactory.request = function (config) {

    let token = AuthToken.getToken();

    if(token)config.headers['x-access-token'] = token;

    return config;
  };

  return authInterceptorsFactory;

});