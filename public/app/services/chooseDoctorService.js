angular.module('userServices', [])
  .factory('chooseDoctorService', function () {

    let doctorsArray = [];
    let selectedDoctor = {};

    return {
      set: setObject,
      get: getObject,
      getSelectedDoctor: getSelectedDoctor,
      setSelectedDoctor: setSelectedDoctor
    };

    function getSelectedDoctor() {
      return selectedDoctor;
    }

    function setSelectedDoctor(doctor) {
      selectedDoctor = doctor;
    }

    function getObject() {
      return doctorsArray;
    }

    function setObject(array) {
      doctorsArray = array;
    }

  });
