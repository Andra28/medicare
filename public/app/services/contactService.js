userApp.service('contactUsService', function ($http) {
  this.postMessages = function (object) {
    return $http.post('api/contact-us', object);
  }

  this.getMessages = function (object) {
    return $http.get('api/contact-us');
  }
})