
jQuery(document).ready(function() {
	
    /*
        Background slideshow
    */
    $('.testimonials-container').backstretch("assets/img/1.jpg");
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(){
    	$('.testimonials-container').backstretch("resize");
    });
	
});
