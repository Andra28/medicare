// call the packages we need
var express = require('express');
var app = express();
var port = process.env.PORT || 8080; //set the application port
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var router = express.Router();
var appRoutes = require('./app/routes/api')(router);//tells the app to use the router object with this route file
var path = require('path');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(morgan('dev'));//start logging our request
//start parsing the data
app.use(bodyParser.json()); //for parsing application to json
app.use(bodyParser.urlencoded({extended: true})); //for parsing application/x-www-form-url-encoded
//use the routes
app.use(express.static(__dirname + '/public'));//express serving the server file location,the front-end will have access to the public folder
app.use('/api', appRoutes);//use the route 'http://localhost:8080/users' but apply api in front of it
//http://localhost:8080/api/users - used for back-end routes


var options = {
    // db: { native_parser: true },
    // server: { poolSize: 5 },
    // replset: { rs_name: 'myReplicaSetName' },
    // user: 'andra',
    // pass: 'awake'
};

//declare the address for the local database
var databaseMap = {
  'localhost':'mongodb://localhost:27017/data'
};

//connect to the local database
mongoose.connect(databaseMap['localhost'],options, function (err) {
  if (err) {
    console.log('Not connected to the database: ' + err);
  } else {
    console.log('Successfully connected to MongoDB');
  }
});

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/app/index.html'));//responds sending a file, taking the current path and joining with this folder
});
//no matter what the user types will get the index page

app.listen(port, function () {
  console.log('Running the server on port ' + port);
});

